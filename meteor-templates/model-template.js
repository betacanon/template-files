/*    model.${NAME}.js
    Models are the only place in the whole system where we're allowed to directly invoke MongoDB calls (create, remove, update, upsert, find, findOne, etc).
    The restricted MODEL object is exposed to global scope so that users can interact with the database through functions you define here.
 */

import { Model } from "/imports/models/model.js";
import { Enum } from "meteor/jagi:astronomy";

/*  DEFINE COLLECTION.
    Define an instance of Model and a schema for a collection in the DB.
    This will be private to this file.
 */
MODEL.${NAME} = new Model({
    name: "${NAME}",
    collection: new Mongo.Collection("${NAME}"),
    fields: {
        name: String, // Example Only
        userId: String, // Example Only
    },
});

/*  PUBLISH FUNCTIONS.
    Publish real-time data from server to client. Access with MODEL.subscribe('<functionName>') or MODEL.subscribeWithoutAutorun('<functionName>') depending on your use case.
    Exactly the same as Meteor.publish(), except `this.DB` is how you access the relevant collection and you don't have to wrap it in Meteor.isServer.
 */
MODEL.${NAME}.publish({
    ${NAME}ForCurrentUser: function (params) {
        // Example Only. Subscribe to this data source with MODEL.subscribe("${NAME}ForCurrentUser", {})

        const userId = this.userId;

        if(!userId)
            return;

        return this.DB.find({userId: userId},
            {
                fields: {
                    name: 1,
                    userId: 1,
                },
            }
        );
    },
});

/*  SHARED MODEL FUNCTIONS (CLIENT + SERVER).
    Define functions to _retrieve_ data. Subscribe first
    The MODEL object can be accessed anywhere and so will shared functions.
 */

MODEL.${NAME}.defineSharedFunctions({
    getById: function ({itemId}) {
        // Example Only. Call this function from anywhere with MODEL.${NAME}.getById({itemId: 'aBcd123Xyz4'});

        if(!itemId)
            return;

        return this.DB.findOne({_id: itemId});
    },
});

/*  SERVER-ONLY MODEL FUNCTIONS.
    Define functions to _modify_ data. This is "trusted" code, meaning that it is protected from users (until you create a Meteor method, that is).
    The MODEL object can be accessed anywhere, but these functions will only be available on the server.
 */
MODEL.${NAME}.defineServerOnlyFunctions({
    create({name, userId}) {
        // Example Only. Call this function from the server with MODEL.${NAME}.create({name: 'Example', userId: '4zyX321dcBa'});

        const insertObj = {
            name: name,
            userId: userId,
        };

        return this.DB.insert(insertObj);
    },
});
