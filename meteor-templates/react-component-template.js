import React, {Component} from 'react';
import {withTracker} from "meteor/react-meteor-data";

// Be sure to import any templates that are needed by this function.
import PartialLoadingSpinner from "/client/components/partials/PartialLoadingSpinner";


class ${NAME} extends Component {

	constructor() {
		super();

		/*  DEFINE DEFAULT STATE VALUES.
			A React component's state variables (accessed with `this.state` or `this.setState()`) will re-run a template's `render` function when they change.
			Set default state values in the `constructor()` function below.
		 */

		this.state = {
			isSubmitting: false, // Set state to "true" with `this.setState({isSubmitting: true})` when a submit button is pressed, for instance. That way you can put a loading spinner in the submit button and prevent multiple form submissions.
			// Set additional state variables here.
		}
	}

	componentDidMount() {
		/*  WHEN A COMPONENT IS CREATED...
			The componentDidMount function runs when a component is created. Be careful! Most components won't have data right when they are created.
			This can be a good place to add a Meteor.call in order to retrieve initial data.
		 */

		// TODO: Either use or delete this example of retrieving data via Meteor Method and setting a state variable with the result.
		// Meteor.call('exampleMeteorMethod', {foo: 'bar'}, (err, res) => {
		// 	if(err)
		// 		alert(err.reason);
		// 	else
		// 		this.setState({foo: res});
		// });
	}

	componentDidUpdate() {
		/*  WHEN A COMPONENT'S VARIABLES ARE CHANGED...
			The componentDidUpdate function runs when a component's state and props are changed.
			Infinite loop risk! Use console.logs inside this function while developing because retrieving and setting state variables here can cause an endless loop. Be sure to remove those console.logs later.
		 */


	}

	componentWillUnmount() {
		/*  WHEN A COMPONENT IS BEING DESTROYED...
			The componentWillUnmount function runs when a component is being destroyed.
		 */


	}

	render() {
		/*  TO OUTPUT HTML...
			The render function runs when the reactive variables within it (props, state, ReactiveVar, Model Functions, etc) are changed.
			Warning! When execution errors occur within a render function, they can cause a dreaded WHITE SCREEN FAILURE. Make sure that variables exist by checking their values OR using Optional Chaining operators on objects (such as `userDoc?.fullName` which resolves to "undefined" if `userDoc` is not defined, preventing an error). Then handle your errors gracefully.
		 */

		const { doneLoading } = this.props;
		const { isSubmitting } = this.state;

		return (
			// TODO: Replace <<type>> with the type of this template:  layout, page, block, listing, modal, partial or field.
			<div className="<<type>> ${NAME}">

				{/* Example HTML with loading fallback below. Revise or delete as necessary. */}
				{
					doneLoading
						?
							<span>${NAME} Component Loaded.</span>
						:
							<PartialLoadingSpinner />
				}

			</div>
		);
	}
}

/*  SUBSCRIBE TO REAL-TIME DATA.
    Code that runs inside of a `withTracker` function will re-run when data sources and reactive variables change.
    Use this to get the EXACT data that you need before fully rendering a template.
 */

export default withTracker((params) => {
	const subscription = Meteor.subscribe(''); // Example only. Subscribe to a Publish function from a model file.
	const subscription2 = Meteor.subscribe(''); // Example only. You can add multiple subscribe functions to a template.

	return {
		doneLoading: subscription.ready() && subscription2.ready(), // Use this for loading states
		// establishments: MODEL.establishments.getIncludedEstablishments()
		...params, // Original params should be returned as well as new params.
	};
})(${NAME});
