/*  utils.${NAME}.js
    Utils are globally-available functions for specialized needs.
    We create utils for processing strings, arrays and other data types.
    Create a single util file just for communication each a third-party integration (such as Stripe or AWS).
    We may also create utils for complicated logic that would not make sense in a Model Function.
 */

// TODO: Import this util into main.client.js (for client-only), main.server.js (for server only) or utils.shared.js (for both client and server usage).

UTILS.${NAME} = {
    exampleUtil(value = '') {

        // Example only. Call this function with `UTILS.${NAME}.exampleUtil(value)` after importing this util file.
        
        if(!value || typeof value !== 'string')
            return '';

        value = value.replace(',', ' ');

        return value;
    },
}
