/*  methods.${NAME}.js
    Meteor Methods are the only way for client-side code to access non-real-time data from the server.
    Security warning! Almost every Meteor Method must validate user permissions, otherwise a security hole is created.
    Meteor Method files should always be saved in the /server directory. They are automatically imported by Meteor.
    Meteor Methods should never be called by the server. Instead, put your logic in a Model Function (or Util Function), call that Model Function from within the Meteor Method, and then other server-side resources can access the same logic without validation required.
 */

Meteor.methods({

    // TODO: Replace the example Meteor Method below (exampleCreateMethod) with your own code.

    exampleCreateMethod({title, description, type, companyId}) {
        //  0. Call this example Meteor Method from the client with `Meteor.call('exampleCreateMethod', {title, description, type, companyId}, (err, res) => { })`

        /*  1. SANITIZE ARGUMENTS.
            Each Meteor Method should sanitize the data provided to it for security and data integrity.
         */
        title = UTILS.strings.sanitizeName(title);
        description = UTILS.strings.removePerimiterSpaces(description);
        type = type.toLowerCase();


        /*  2. VERIFY USER PERMISSIONS.
            Always make sure that the user requesting this content is authorized to create, read, update or delete the content. Otherwise we have a security hole.
         */

        const userDoc = MODEL.users.getCurrentUser();
        const companyDoc = MODEL.companies.getById(companyId);
        const userCanManageCompany = MODEL.companies.canUserManageCompany({userDoc, companyDoc});

        if(!userDoc)
			throw new Meteor.Error('no-user', 'You must be logged in to perform this action.');
        if(companyDoc)
            throw new Meteor.Error('invalid-company', 'There was an issue accessing data for this action. Please try again.');
        if(!userCanManageCompany)
            throw new Meteor.Error('not-authorized', 'We were unable to authorize your user account with this company.');


        /*  3. TAKE ACTION.
            Once the data is sanitized and the user is authorized, leverage a Model Function or a Util Function to perform the action and maximize code re-use.
            Ideally this will only require one line of code in the actual Meteor Method function.
         */

        return MODEL.courses.create({title, description, type, companyId});
    },

});
